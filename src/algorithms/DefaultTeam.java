package algorithms;

import java.awt.Point;
import java.util.ArrayList;

public class DefaultTeam {
    public ArrayList<Point> calculFVS(ArrayList<Point> points, int edgeThreshold) {
        test_has_cycle();
        //ArrayList<Point> fvs = gloutonus(points, edgeThreshold);
        ArrayList<Point> fvs = approximus(points, edgeThreshold);
        //assert(Evaluation.isValide(points, fvs, edgeThreshold));
        return fvs;
    }

    static <T> T swap_remove(ArrayList<T> array, int index) {
        int end = array.size() - 1;
        if (index == end) {
            return array.remove(end);
        } else {
            T swapped = array.remove(end);
            T removed = array.get(index);
            array.set(index, swapped);
            return removed;
        }
    }

    static <T> T pop(ArrayList<T> array) {
        int end = array.size() - 1;
        return array.remove(end);
    }

    static <T> void swap_back(ArrayList<T> array, int index, T inserted) {
        int end = array.size() - 1;
        if (index > end) {
            array.add(inserted);
        } else {
            T swapped = array.get(index);
            array.set(index, inserted);
            array.add(swapped);
        }
    }

    ArrayList<Point> local_searchus(ArrayList<Point> rest, ArrayList<Point> fvs, int threshold) {
        for (int i = 0; i < fvs.size(); i++) {
            Point pi = swap_remove(fvs, i);
            for (int j = i; j < fvs.size(); j++) {
                Point pj = swap_remove(fvs, j);
                for (int k = 0; k < rest.size(); k++) {
                    Point pk = swap_remove(rest, k);
                    rest.add(pi);
                    rest.add(pj);
                    fvs.add(pk);

                    if (!has_cycle(rest, threshold)) {
                        return local_searchus(rest, fvs, threshold);
                    }

                    pk = pop(fvs);
                    pj = pop(rest);
                    pi = pop(rest);
                    swap_back(rest, k, pk);
                }
                swap_back(fvs, j, pj);
            }
            swap_back(fvs, i, pi);
        }
        return fvs;
    }

    ArrayList<Point> gloutonus(ArrayList<Point> points, int threshold) {
        ArrayList<Point> rest = (ArrayList<Point>) points.clone();
        ArrayList<Point> fvs = new ArrayList<Point>();

        while (has_cycle(rest, threshold)) {
            //while (!Evaluation.isValide(points, fvs)) {
            int maxi = 0;
            int maxdeg = 0;
            for (int i = 0; i < rest.size(); i++) {
                int deg = 0;
                for (int j = 0; j < rest.size(); j++) {
                    if (has_edge(rest, i, j, threshold)) {
                        deg++;
                    }
                }
                if (deg > maxdeg) {
                    maxi = i;
                    maxdeg = deg;
                }
            }

            fvs.add(rest.get(maxi));
            rest.remove(maxi);
        }

        //return fvs;
        return local_searchus(rest, fvs, threshold);
    }

    boolean has_cycle(ArrayList<Point> points, int threshold) {
        ArrayList<Integer> visited = new ArrayList<Integer>(points.size());
        for (int i = 0; i < points.size(); i++) {
            visited.add(null);
        }

        for (int i = 0; i < points.size(); i++) {
            if (visited.get(i) == null) {
                visited.set(i, i);
                if (has_cycle_visit(i, points, visited, threshold)) {
                    return true;
                }
            }
        }

        return false;
    }

    boolean has_cycle_visit(int i, ArrayList<Point> points, ArrayList<Integer> visited, int threshold) {
        for (int j = 0; j < points.size(); j++) {
            if (has_edge(points, i, j, threshold)) {
                if (visited.get(j) == null) {
                    visited.set(j, i);
                    if (has_cycle_visit(j, points, visited, threshold)) {
                        return true;
                    }
                } else if (visited.get(i) != j) {
                    return true;
                }
            }
        }

        return false;
    }

    boolean has_edge(ArrayList<Point> points, int i, int j, int threshold) {
        return i != j && points.get(i).distanceSq(points.get(j)) < threshold*threshold;
    }

    void test_has_cycle() {
        ArrayList<Point> no_cycle = new ArrayList<>();
        no_cycle.add(new Point(50, 50));
        no_cycle.add(new Point(50, 100));
        no_cycle.add(new Point(50, 150));
        test(!has_cycle(no_cycle, 100));

        ArrayList<Point> cycle = new ArrayList<>();
        cycle.add(new Point(50, 50));
        cycle.add(new Point(50, 100));
        cycle.add(new Point(100, 50));
        test(has_cycle(cycle, 100));
    }

    void test(boolean b) {
        if (b) {
            System.out.println("passed");
        } else {
            System.out.println("failed");
        }
    }

    public ArrayList<Point> approximus(ArrayList<Point> points, int threshold) {
        ArrayList<Point> stack = new ArrayList<Point>();
        ArrayList<Point> not_in_stack = new ArrayList<Point>();
        ArrayList<Point> remaining = new ArrayList<Point>(points.size());
        ArrayList<Integer> degrees = compute_degrees(points, threshold);
        ArrayList<Float> weights = new ArrayList<Float>(points.size());
        for (int i = 0; i < points.size(); i++) {
            remaining.add(points.get(i));
            weights.add(1.0f);
        }

        while (true) {
            { // cleanup
                boolean changed = true;
                while (changed) {
                    changed = false;
                    int i = 0;
                    while (i < remaining.size()) {
                        if (degrees.get(i) <= 1) {
                            // cannot participate in a cycle
                            not_in_stack.add(swap_remove(remaining, degrees, weights, i, threshold));
                            changed = true;
                        } else {
                            i++;
                        }
                    }
                }
            }
            if (remaining.isEmpty()) {
                break;
            }

            if (has_semidisjoint_cycle(remaining, degrees, threshold)) {
                float min_weight = array_min(weights);
                for (int i = 0; i < remaining.size(); i++) {
                    weights.set(i, weights.get(i) - min_weight);
                }
            } else {
                int i = 0;
                float min_ratio = weights.get(i)/(degrees.get(i) - 1);
                for (i = 1; i < remaining.size(); i++) {
                    float ratio = weights.get(i)/(degrees.get(i) - 1);
                    if (ratio < min_ratio) {
                        min_ratio = ratio;
                    }
                }
                for (i = 0; i < remaining.size(); i++) {
                    weights.set(i, weights.get(i) - (min_ratio * (degrees.get(i) - 1)));
                }
            }

            int i = 0;
            while (i < remaining.size()) {
                if (weights.get(i) <= 0.00001) {
                    stack.add(swap_remove(remaining, degrees, weights, i, threshold));
                } else {
                    i++;
                }
            }
        }


        for (int i = stack.size() - 1; i >= 0; i--) {
            not_in_stack.add(swap_remove(stack, i));
            if (has_cycle(not_in_stack, threshold)) {
                swap_back(stack, i, pop(not_in_stack));
            }
        }
        return stack;
    }

    public Point swap_remove(
        ArrayList<Point> remaining,
        ArrayList<Integer> degrees,
        ArrayList<Float> weights,
        int index,
        int threshold
    ) {
        for (int i = 0; i < remaining.size(); i++) {
            if (has_edge(remaining, index, i, threshold)) {
                degrees.set(i, degrees.get(i) - 1);
            }
        }

        swap_remove(degrees, index);
        swap_remove(weights, index);
        return swap_remove(remaining, index);
    }

    public ArrayList<Integer> compute_degrees(ArrayList<Point> points, int threshold) {
        ArrayList<Integer> degrees = new ArrayList<Integer>(points.size());
        for (int i = 0; i < points.size(); i++) {
            int degree = 0;
            for (int j = 0; j < points.size(); j++) {
                if (has_edge(points, i, j, threshold)) {
                    degree++;
                }
            }
            degrees.add(degree);
        }
        return degrees;
    }

    public boolean has_semidisjoint_cycle(ArrayList<Point> remaining, ArrayList<Integer> degrees, int threshold) {
        if (has_cycle(remaining, threshold)) {
            boolean not_two = false;
            for (int i = 0; i < degrees.size(); i++) {
                if (degrees.get(i) != 2) {
                    if (not_two) {
                        return false;
                    } else {
                        not_two = true;
                    }
                }
            }
            return true;
        } else {
            return false;
        }
    }

    // non empty array
    public float array_min(ArrayList<Float> array) {
        int i = 0;
        float min = array.get(i);
        for (i = 1; i < array.size(); i++) {
            float elem = array.get(i);
            if (elem < min) {
                min = elem;
            }
        }
        return min;
    }
}
